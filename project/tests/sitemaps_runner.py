from crawler import CrawlerWithResults
from scrapy.crawler import CrawlerProcess
from spiders.sitemaps import LocalSitemapsSpider

cp = CrawlerProcess()

c1 = CrawlerWithResults(LocalSitemapsSpider)
cp.crawl(c1)
c1.spider.sitemap_urls = [c1.spider.local_dir + c1.spider.website_folder + "/sitemap1.xml"]
c1.spider.name += '1'

c2 = CrawlerWithResults(LocalSitemapsSpider)
cp.crawl(c2)
c2.spider.sitemap_urls = [c1.spider.local_dir + c1.spider.website_folder + "/sitemap2.xml"]
c2.spider.name += '2'

cp.start()

c1.items
c2.items
