import unittest
from spiders import jobs
from response import fake_response_from_file


class SpiderTests(unittest.TestCase):
    def setUp(self):
        self.html_spider = jobs.JobsSpider()

    def _test_results_count(self, results, expected_length):
        count = sum(1 for x in results)
        self.assertEqual(count, expected_length)
        return True

    def test_parse_1(self):
        results = self.html_spider.parse(fake_response_from_file('sample1.html'))
        self._test_results_count(results, 120)


if __name__ == "__main__":
    unittest.main()