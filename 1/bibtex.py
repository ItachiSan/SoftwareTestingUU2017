def extract_author(author):
    # If we have a comma, treat it properly
    if ',' in author:
        words = author.split(',')
        surname, name = words[0].strip(), words[1].strip()
        return (surname, name)

    # If we have many words splitted by a space...
    words = author.split(' ')
    if len(words) > 1:
        # Split them and get name and surname
        surname, name = words[-1], ' '.join(words[:-1])
        # Return them then
        return (surname, name)

    # Else, only a surname (or similar)
    return (author, '')

def extract_authors(authors):
    return [extract_author(x.strip()) for x in authors.split('and')]