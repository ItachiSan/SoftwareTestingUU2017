# Code diary
## Initial setup
All the tests are executed after the following setup function:
```python
def setUp(self):
    self.simple_author_1 = "Smith"
    self.simple_author_2 = "Jones"
    self.author_1 = "John Smith"
    self.author_2 = "Bob Jones"
    self.author_3 = "Justin Kenneth Pearson"
    self.surname_first_1 = "Pearson, Justin Kenneth"
    self.surname_first_2 = "Van Hentenryck, Pascal"
    self.multiple_authors_1 = "Pearson, Justin and Jones, Bob"
```
So, all the data used in the tests will have the above values.

## Test 1
### RED
The first test checks only for author surnames.
```python
def test_author_1(self):
    # Test only surnames.
    (Surname, FirstNames) = bibtex.extract_author(self.simple_author_1)
    self.assertEqual((Surname, FirstNames),  ('Smith', ''))
    (Surname, FirstNames) = bibtex.extract_author(self.simple_author_2)
    self.assertEqual((Surname, FirstNames),  ('Jones', ''))
```
### GREEN
We can just write a function which return the given surname and an empty string
as a tuple. In this way, the format is respected.
```python
def extract_author(author):
    return (author, '')
```

## Test 2
### RED
The initial method fails for the second test with a
```python
======================================================================
FAIL: test_author_2 (__main__.TestAuthorExtract)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "e:/Documenti/Uppsala/Courses/2/Software Testing/Assignments/1/bibtex_test.py", line 32, in test_author_2
    self.assertEqual((Surname, First), ("Smith", "John"))
AssertionError: Tuples differ: ('John Smith', '') != ('Smith', 'John')

First differing element 0:
'John Smith'
'Smith'

- ('John Smith', '')
+ ('Smith', 'John')
```
The test is formulated as
```python
def test_author_2(self):
    # Test simple firstname author.
    (Surname, First) = bibtex.extract_author(self.author_1)
    self.assertEqual((Surname, First), ("Smith", "John"))
    (Surname, First) = bibtex.extract_author(self.author_2)
    self.assertEqual((Surname, First), ("Jones", "Bob"))
```
The problem is that the initial implementation of the function consider
everything as a surname.
### GREEN
We can check if we have both name and surname, which are divided by a space; if
we have both, we return them properly splitted, else we reuse the previous case.
```python
def extract_author(author):
    # If we have many words splitted by a space...
    words = author.split(' ')
    if len(words) > 1:
        # Split them and get name and surname
        surname, name = words[1], words[0]
        # Return them then
        return (surname, name)
    else:
        # Only a surname (or similar)
        return (author, '')
```

## Test 3
### RED
The following test fails
```python
def test_author_3(self):
    (Surname, First) = bibtex.extract_author(self.author_3)
    self.assertEqual((Surname, First), ("Pearson", "Justin Kenneth"))
```
with the following output
```python
======================================================================
FAIL: test_author_3 (__main__.TestAuthorExtract)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "e:/Documenti/Uppsala/Courses/2/Software Testing/Assignments/1/bibtex_test.py", line 38, in test_author_3
    self.assertEqual((Surname, First), ("Pearson", "Justin Kenneth"))
AssertionError: Tuples differ: ('Kenneth', 'Justin') != ('Pearson', 'Justin Kenneth')

First differing element 0:
'Kenneth'
'Pearson'

- ('Kenneth', 'Justin')
+ ('Pearson', 'Justin Kenneth')
```
This is because we take, given the previous implementation, the second word as
surname, while people can also have second names.
### GREEN
Instead of picking up the second word, we can return the last one as surname
and everything else as name.
```python
def extract_author(author):
    # If we have many words splitted by a space...
    words = author.split(' ')
    if len(words) > 1:
        # Split them and get name and surname
        surname, name = words[-1], ' '.join(words[:-1])
        # Return them then
        return (surname, name)
    else:
        # Only a surname (or similar)
        return (author, '')
```

## Test 4
### RED
The following test fails
```python
def test_surname_first(self):
    (Surname, First) = bibtex.extract_author(self.surname_first_1)
    self.assertEqual((Surname, First), ("Pearson", "Justin Kenneth"))
    (Surname, First) = bibtex.extract_author(self.surname_first_2)
    self.assertEqual((Surname, First), ("Van Hentenryck", "Pascal"))
```
with the following output
```python
======================================================================
FAIL: test_surname_first (__main__.TestAuthorExtract)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "e:/Documenti/Uppsala/Courses/2/Software Testing/Assignments/1/bibtex_test.py", line 42, in test_surname_first
    self.assertEqual((Surname, First), ("Pearson", "Justin Kenneth"))
AssertionError: Tuples differ: ('Kenneth', 'Pearson, Justin') != ('Pearson', 'Justin Kenneth')

First differing element 0:
'Kenneth'
'Pearson'

- ('Kenneth', 'Pearson, Justin')
+ ('Pearson', 'Justin Kenneth')
```
In this case the test fails because we don't recognize one pattern:

* NAME SURNAME
* SURNAME, NAME <- This one

We can initially check if we have a comma; if the comma is present, we can
handle it as a different case from the previous ones.
```python
def extract_author(author):
    # If we have a comma, treat it properly
    if ',' in author:
        words = author.split(',')
        surname, name = words[0], words[1]
        return (surname, name)

    # If we have many words splitted by a space...
    words = author.split(' ')
    if len(words) > 1:
        # Split them and get name and surname
        surname, name = words[-1], ' '.join(words[:-1])
        # Return them then
        return (surname, name)
    else:
        # Only a surname (or similar)
        return (author, '')
```

### RED, still
The test however still fails
```python
======================================================================
FAIL: test_surname_first (__main__.TestAuthorExtract)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "e:/Documenti/Uppsala/Courses/2/Software Testing/Assignments/1/bibtex_test.py", line 42, in test_surname_first
    self.assertEqual((Surname, First), ("Pearson", "Justin Kenneth"))
AssertionError: Tuples differ: ('Pearson', ' Justin Kenneth') != ('Pearson', 'Justin Kenneth')

First differing element 1:
' Justin Kenneth'
'Justin Kenneth'

- ('Pearson', ' Justin Kenneth')
?              -

+ ('Pearson', 'Justin Kenneth')
```
The space after the comma has to be removed; to do this consistently, we can
use the `strip` function, which removes leading and trailing whitespaces from
the string.
```python
def extract_author(author):
    # If we have a comma, treat it properly
    if ',' in author:
        words = author.split(',')
        surname, name = words[0].strip(), words[1].strip()
        return (surname, name)

    # If we have many words splitted by a space...
    words = author.split(' ')
    if len(words) > 1:
        # Split them and get name and surname
        surname, name = words[-1], ' '.join(words[:-1])
        # Return them then
        return (surname, name)

    # Else, only a surname (or similar)
    return (author, '')
```

## Test 5
### RED
We are now required to have a new function, `extract_authors`, which can handle
the BibTeX keyword `and`.
```python
def test_multiple_authors(self):
    Authors = bibtex.extract_authors(self.multiple_authors_1)
    self.assertEqual(Authors[0],  ('Pearson', 'Justin'))
    self.assertEqual(Authors[1], ('Jones', 'Bob'))
```

### GREEN
Using a (cool) list comprehension, `strip` and our function `extract_author`,
we can elegantly write the following function:
```python
def extract_authors(authors):
    return [extract_author(x.strip()) for x in authors.split('and')]
```
which makes the last test to pass.