# Software Testing UU 2017
This repository keeps track of my Software Testing assignments and, probably,
of the project also.

Notice: on Windows, `pywin32` is required. As it is not stable, we have to stick
to an older version packaged for *pip*; the command to use is
`pip install pypiwin32 < 222`